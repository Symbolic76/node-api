import listModel from '../models/list';

export default {
	create : async (label) => {
		let list = new listModel();
        list.label = label;
	    
	    await list.save()
		
		return list;
	},
	remove : function(listId) 
	{
		listModel.remove({_id: listId}, function(err){
	        if (err)
	        {
		        throw Error(err);
	        }
	    }); 
	},
	findAll: function(callback) 
	{
		listModel.find({}, function(err, docs) {
		    if (!err){ 
		        callback(docs);
		    } 
		    else 
		    {
			    throw err;
			}
		});
	}
}