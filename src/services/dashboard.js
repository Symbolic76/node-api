import dashboardModel from '../models/dashboard';

export default {
	create : async (label,userIds) => {
		let dashboard = new dashboardModel();
        dashboard.label = label;
        dashboard.userIds = userIds;
	    
	    await dashboard.save()
		
		return dashboard;
	},
	remove : function(dashboardId) 
	{
		dashboardModel.remove({_id: dashboardId}, function(err){
	        if (err)
	        {
		        throw Error(err);
	        }
	    }); 
	},
	findAll: async() =>
	{
        return await dashboardModel.find({});
	}
}