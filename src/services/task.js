import taskModel from '../models/task';

export default {
	create : async (label, description, dueDate, statusId, listId) => {
		let task = new taskModel();
        task.label = label;
        task.description = description;
        task.dueDate = dueDate;
        task.statusId = statusId;
        task.listId = listId;
        task.createdDate = Date.now();
        task.updatedDate = Date.now(); 
	    
	    await task.save()
		
		return task;
	},
	remove : function(taskId) 
	{
		taskModel.remove({_id: taskId}, function(err){
	        if (err)
	        {
		        throw Error(err);
	        }
	    }); 
	},
	findAll: function(callback) 
	{
		taskModel.find({}, function(err, docs) {
		    if (!err){ 
		        callback(docs);
		    } 
		    else 
		    {
			    throw err;
			}
		});
	}
}