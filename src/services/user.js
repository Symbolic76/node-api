import userModel from '../models/user';

export default {
	create : async (email, password, createdDate,updatedDate) => {
		let user = new userModel();
	    user.email 	= email;
        user.password 	= password;
        user.createdDate		= Date.now();
        user.updatedDate        = Date.now();
	    
	    await user.save()
		
		return user;
	},
	remove : function(userId) 
	{
		userModel.remove({_id: userId}, function(err){
	        if (err)
	        {
		        throw Error(err);
	        }
	    }); 
	},
	findAll: function(callback) 
	{
		userModel.find({}, function(err, docs) {
		    if (!err){ 
		        callback(docs);
		    } 
		    else 
		    {
			    throw err;
			}
		});
	}
}