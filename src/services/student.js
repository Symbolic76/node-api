import studentModel from '../models/student';

export default {
	create : async (lastName, firstName, age) => {
		let student = new studentModel();
	    student.lastName 	= lastName;
	    student.firstName 	= firstName;
	    student.age 		= age;
	    
	    await student.save()
		
		return student;
	},
	remove : function(studentId) 
	{
		studentModel.remove({_id: studentId}, function(err){
	        if (err)
	        {
		        throw Error(err);
	        }
	    }); 
	},
	findAll: function(callback) 
	{
		studentModel.find({}, function(err, docs) {
		    if (!err){ 
		        callback(docs);
		    } 
		    else 
		    {
			    throw err;
			}
		});
	}
}