import { Container } 	from 'typedi';
import LoggerInstance 	from './logger';
import config 			from '../config';
import userService from "../services/user";

export default () =>
{
	try
	{
		Container.set('logger', LoggerInstance);
		Container.set('userService', userService);

		LoggerInstance.info('✌️ Logger injected into container');

		return;
	}
	catch (e)
	{
		LoggerInstance.error('🔥 Error on dependency injector loader: %o', e);
		throw e;
	}
};
