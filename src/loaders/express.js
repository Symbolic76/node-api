import session		from 'express-session';
import bodyParser 	from 'body-parser';

import apiRoutes 	from '../api';
import frontRoutes	from '../front';
import config 		from '../config';

export default (app) => {

  app.use(bodyParser.urlencoded({extended : true}));
  app.use(bodyParser.json());
  
  app.use(session({
	secret: config.jwtSecret,
	resave: true,
	saveUninitialized: true
  }));
  
  app.use(config.api.prefix, apiRoutes());
  app.use('', frontRoutes());
  /// catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err['status'] = 404;
    next(err);
  });

};