import mongoose from 'mongoose';

const userSchema = mongoose.Schema({
    email: String,
    password: String,
    createdDate: Date,
    updatedDate: Date,
}); 

export default mongoose.model('User', userSchema);