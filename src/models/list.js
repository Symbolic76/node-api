import mongoose from 'mongoose';

const listSchema = mongoose.Schema({
    dashboardId: [{ type: mongoose.Types.ObjectId, ref: 'Dashboard' }],
    label: String,
}); 

export default mongoose.model('List', listSchema);