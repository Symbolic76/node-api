import mongoose from 'mongoose';

const statusSchema = mongoose.Schema({
    label: String,
    color: String,
}); 

export default mongoose.model('Status', statusSchema);