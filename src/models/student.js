import mongoose from 'mongoose';

const studentSchema = mongoose.Schema({
    lastName: String, 
    firstName: String, 
    age: Number,
    //classRoom: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Classroom' }],
}); 

export default mongoose.model('Student', studentSchema);
