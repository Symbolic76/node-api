import mongoose from 'mongoose';

const dashboardSchema = mongoose.Schema({
    label: String,
    userIds: [{ type: mongoose.Types.ObjectId, ref: 'User' }],
}); 

export default mongoose.model('Dashboard', dashboardSchema);