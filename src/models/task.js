import mongoose from 'mongoose';

const taskSchema = mongoose.Schema({
    label: String,
    description: String,
    dueDate: Date,
    statusId: [{ type: mongoose.Types.ObjectId, ref: 'Status' }],
    listId: [{ type: mongoose.Types.ObjectId, ref: 'List' }],
    createdDate: Date,
    updatedDate: Date,
}); 

export default mongoose.model('Task', taskSchema);