import { Router } 	from 'express';


export default () => {
	const app = Router();
	
	app.get('/', (req, res) => {
        res.send({message : 'Bravo, lélève est maintenant stockée en base de données'});
	});   
	
	return app
}