import { Router } 	from 'express';
import { Container } 								from 'typedi';
import middlewares 									from '../middlewares';
import { celebrate, Joi } 							from 'celebrate';
import config from '../../config';

const route = Router();

export default (app) => 
{
	app.use('/auth', route);
     
	route.post(
		'/simple-register',
		celebrate(
		{
			body: Joi.object({
				uniqId		: Joi.string().required(),                                                                          
			}),
		}),
		async (req, res, next) => 
		{
			const logger = Container.get('logger');
			
			logger.debug('Calling Sign-Up endpoint with body: %o', req.body );
			
			try 
			{
				const _localRole = [config.roles.user];
				const _localData = { uniqId : req.body.uniqId, email: req.body.uniqId + '@mail.com', roles: JSON.stringify(_localRole), password: req.body.uniqId };
				const authServiceInstance 	= Container.get(AuthService);
				const { user, token } 		= await authServiceInstance.create(_localData);
				return res.status(201).json({ user, token });
			} 
			catch (e) 
			{
				logger.error('🔥 error: %o', e);
				return next(e);
			}
		},
	);
	
	route.post('/logout', middlewares.isAuth, (req, res, next) => 
	{
		const logger = Container.get('logger');
		logger.debug('Calling Sign-Out endpoint with body: %o', req.body);
		
		try 
		{
			//@TODO AuthService.Logout(req.user) do some clever stuff
			return res.status(200).end();
		} 
		catch (e) 
		{
			logger.error('🔥 error %o', e);
			return next(e);
		}
	});
};